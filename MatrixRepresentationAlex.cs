﻿/*
 * Author: Alex Nadal 
 * Date: 22/09/2022
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M17_UF1_E2_NadalAlex
{
    internal class MatrixRepresentationAlex : MatrixRepresentation
    {
       
        public MatrixRepresentationAlex(int width, int height) : base(width, height)
        {

        }
        //Escriu la matriu i també pinta les cadenes de numeros
        public override void printMatrix()
        {
            Console.Clear();
            for (int y = 0; y < _theMatrix.GetLength(1); y++)
            {
                for (int x = 0; x < _theMatrix.GetLength(0); x++)
                {
                    if (y == _theMatrix.GetLength(1) - 1 && _theMatrix[x, y] != '0' || (y != _theMatrix.GetLength(1) - 1 && _theMatrix[x, y] != '0' && _theMatrix[x, y + 1] == '0'))
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write($" {_theMatrix[x, y]}");
                        Console.ResetColor();
                    }
                    else if(y!=_theMatrix.GetLength(1)-1 && (_theMatrix[x,y+1] != '0' && _theMatrix[x,y] != '0'))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($" {_theMatrix[x, y]}");
                        Console.ResetColor();
                    }
                    else 
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write($" {_theMatrix[x, y]}");
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
