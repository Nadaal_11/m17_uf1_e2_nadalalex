﻿/*
 * Author: Alex Nadal 
 * Date: 22/09/2022
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M17_UF1_E2_NadalAlex
{
    internal class GameEngineAlex : GameEngine
    {
        private MatrixRepresentationAlex _matrix = new MatrixRepresentationAlex(50, 50);

        protected override void Start()
        {
            _matrix.CleanTheMatrix();
        }
        protected override void Update()
        {
            Random random = new Random();
            char[,] matriu = _matrix.TheMatrix;
            
            //Aquest for copia la fila de adalt
            for (int x = 0; x < matriu.GetLength(0); x++)
            {
                for (int y = matriu.GetLength(1)-1; y >= 1; y--)
                {
                    matriu[x, y] = matriu[x, y-1];
                }
            }

            //La primera fila la converteix tot en 0 i genera cadenes de 5
            for (int i = 0; i < matriu.GetLength(1); i++)
            {
                matriu[i, 0] = '0';
                if (matriu[i, 1] != '0')
                {
                    matriu[i, 0] = (char)random.Next(49, 57);
                    if (matriu[i,5] !='0')
                    {
                        matriu[i, 0] = '0';
                    }
                }
            }

            //Genera un numero aleatori a la primera fila
            matriu[random.Next(0, matriu.GetLength(0)), 0] = (char)random.Next(49, 57);
          
            _matrix.clippingMatrix(matriu);
            _matrix.printMatrix();
        }
        protected override void Exit()
        {
            _matrix.CleanTheMatrix();
        }
    }
}
